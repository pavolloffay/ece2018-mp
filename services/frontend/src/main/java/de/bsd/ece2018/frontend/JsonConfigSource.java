/*
 * Copyright 2018 Red Hat, Inc. and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.bsd.ece2018.frontend;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.json.Json;
import javax.json.JsonException;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import org.eclipse.microprofile.config.spi.ConfigSource;

/**
 * @author hrupp
 */
public class JsonConfigSource implements ConfigSource {



  private static final String MY_PROPERTIES_JSON = "my_properties.json";
  private final Map<String, String> props = new HashMap<>();

  public JsonConfigSource() {

    String homeDir = System.getProperty("user.home");

    try (InputStream ins = new FileInputStream(new File(homeDir, MY_PROPERTIES_JSON));){
      JsonReader reader = Json.createReader(ins);
      JsonObject jsonObject = reader.readObject();
      Set<Map.Entry<String, JsonValue>> entries = jsonObject.entrySet();
      for (Map.Entry<String, JsonValue> entry : entries) {
        props.put(entry.getKey(), entry.getValue().toString());
      }
    } catch (IOException e) {
      System.err.println(" Datei nicht gefunden: " + e.getLocalizedMessage());
    } catch (JsonException je) {
      System.err.println("Json nicht parsbar? " + je.getLocalizedMessage());
    }

  }

  @Override
  public int getOrdinal() {
    return 105;
  }

  @Override
  public Map<String, String> getProperties() {
    Map<String, String> out = new HashMap<>(props);
    return out;
  }

  @Override
  public String getValue(String propertyName) {
    return props.get(propertyName);
  }

  @Override
  public String getName() {
    return MY_PROPERTIES_JSON;
  }
}
