/*
 * Copyright 2018 Red Hat, Inc. and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.bsd.ece2018.frontend;

import java.time.temporal.ChronoUnit;
import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.json.JsonObject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.faulttolerance.ExecutionContext;
import org.eclipse.microprofile.faulttolerance.Fallback;
import org.eclipse.microprofile.faulttolerance.FallbackHandler;
import org.eclipse.microprofile.faulttolerance.Retry;
import org.eclipse.microprofile.faulttolerance.Timeout;
import org.eclipse.microprofile.metrics.Counter;
import org.eclipse.microprofile.metrics.annotation.Metric;
import org.eclipse.microprofile.metrics.annotation.Timed;
import org.eclipse.microprofile.rest.client.inject.RestClient;

/**
 * @author hrupp
 */
@RequestScoped
@Path("/")
public class FrontendHandler {

  @Inject
  @RestClient
  BackendService service;

  @Inject
  @ConfigProperty(name = "greeting", defaultValue = "Guten Tag")
  String greeting;

  @Inject
  WorkerBean bean;

  @Inject
  @Metric(absolute = true)
  Counter callCount;

  @GET
  @Timed
  @Fallback(fallbackMethod = "ersatzFuerFehler")
  @Timeout(value = 200L,unit = ChronoUnit.MILLIS)
  @Retry(delay = 100L, maxRetries = 5)
  @RolesAllowed("any")
  public Response doSomething(@QueryParam("mode") String mode)  {

/*
    try {
      Thread.sleep(5000);
    } catch (InterruptedException e) {
      e.printStackTrace();  // TODO: Customise this generated block
    }
*/

    callCount.inc();

    bean.doWork();

      String msg = getMessage(mode);
      return Response.ok( greeting + "[" + callCount.getCount() + "] : " + msg + "\n").build();

  }


  String getMessage(String mode) {


    Response r;

    if (mode != null && mode.toLowerCase().contains("l")) {
      r = service.getFailure();
    }
    else if (mode != null && mode.toLowerCase().contains("ra")) {
      r = service.getRandom();
    } else {
      r = service.getOk();
    }

    JsonObject bla = r.readEntity(JsonObject.class);

    String msg = bla.getString("msg");

    return msg;
   }


   @SuppressWarnings("unused")
   private Response ersatzFuerFehler(String _unused) {
      return Response.ok("@@ fallback @@  [" + callCount.getCount() + "]").build();
   }


   @Dependent
   private static class FallBackClass implements FallbackHandler<Response> {

     @Override
     public Response handle(ExecutionContext context) {
       return Response.ok("!! fallback !!").build();
     }
   }
}
