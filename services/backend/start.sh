#!/usr/bin/env bash
set -x

#if [ ! -e target/backend-thorntail.jar ]
#then
	mvn clean install -Pthorntail
#fi

java -Dswarm.port.offset=1000 -jar target/backend-thorntail.jar
